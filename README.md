# RGB to Hex converter

This little project provides RGB to Hex converter

## RGB to Hex screenshot:
![RGB_to_Hex](/img/rgb_to_hex.png)

## RGB to Hex in action:

https://xitarps.github.io/RGB_to_Hex/